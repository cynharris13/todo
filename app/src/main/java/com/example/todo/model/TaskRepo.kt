package com.example.todo.model

import com.example.todo.model.entity.Task
import com.example.todo.model.local.TaskDB
import javax.inject.Inject

/**
 * Task repository.
 *
 * @property taskDB
 * @constructor Create empty Task repo
 */
class TaskRepo @Inject constructor(private val taskDB: TaskDB) {
    private val taskDAO = taskDB.taskDAO()

    /**
     * Get tasks.
     *
     * @return
     */
    suspend fun getTasks(): List<Task> {
        return taskDAO.getTasks()
    }

    /**
     * Get task.
     *
     * @param id
     * @return
     */
    suspend fun getTask(id: Int): Task {
        return taskDAO.findByID(id)
    }

    /**
     * Add task.
     *
     * @param tasks
     */
    suspend fun addTask(vararg tasks: Task) {
        taskDAO.insertAll(tasks = tasks)
    }

    /**
     * Delete task.
     *
     * @param task
     */
    suspend fun deleteTask(task: Task) {
        taskDAO.delete(task)
    }

    /**
     * Update tasks.
     *
     * @param tasks
     */
    suspend fun updateTasks(vararg tasks: Task) {
        return taskDAO.updateTasks(tasks = tasks)
    }
}
