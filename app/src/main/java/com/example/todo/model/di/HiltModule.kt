package com.example.todo.model.di

import androidx.room.Room
import com.example.todo.ToDoApplication
import com.example.todo.model.local.TaskDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object HiltModule {
    @Provides
    fun providesContactDB(): TaskDB {
        return Room.databaseBuilder(
            ToDoApplication.appContext(),
            TaskDB::class.java,
            "task-database"
        ).build()
    }
}
