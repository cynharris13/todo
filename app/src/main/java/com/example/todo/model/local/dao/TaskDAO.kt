package com.example.todo.model.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.example.todo.model.entity.Task

@Dao
interface TaskDAO {
    @Query("select * from tasks")
    suspend fun getTasks(): List<Task>

    @Transaction
    @Query("SELECT * FROM tasks WHERE tasks.id LIKE :id LIMIT 1")
    suspend fun findByID(id: Int): Task

    @Insert
    suspend fun insertAll(vararg tasks: Task)

    @Delete
    suspend fun delete(task: Task)

    @Update
    suspend fun updateTasks(vararg tasks: Task)
}
