package com.example.todo.model.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.todo.model.entity.Task
import com.example.todo.model.local.dao.TaskDAO

@Database(entities = [Task::class], version = 1)
abstract class TaskDB : RoomDatabase() {
    abstract fun taskDAO(): TaskDAO
}
