package com.example.todo.view.details

import com.example.todo.model.entity.Task

/**
 * Task state.
 *
 * @property isLoading
 * @property task
 * @constructor Create empty Task state
 */
data class TaskState(
    val isLoading: Boolean = false,
    val task: Task? = null
)
