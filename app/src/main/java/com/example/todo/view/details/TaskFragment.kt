package com.example.todo.view.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.todo.model.entity.Task
import com.example.todo.ui.theme.ToDoTheme
import com.example.todo.viewmodel.TaskViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 * Use the [TaskFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class TaskFragment : Fragment() {
    private val args by navArgs<TaskFragmentArgs>()
    private val taskViewModel by viewModels<TaskViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val taskID = args.taskID
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by taskViewModel.taskState.collectAsState()
                taskViewModel.getTask(taskID)
                ToDoTheme() {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        if (state.isLoading) { CircularProgressIndicator() } else {
                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                                Text(text = state.task?.name ?: "No name")
                                Text(text = state.task?.description ?: "No description")
                                val checkedState = remember { mutableStateOf(false) }
                                Checkbox(
                                    checked = checkedState.value,
                                    onCheckedChange = {
                                        checkedState.value = it
                                        taskViewModel.updateTask(
                                            Task(taskID, state.task?.name, state.task?.description, it)
                                        )
                                    }
                                )
                                Button(
                                    onClick = {
                                        taskViewModel.deleteTask(state.task!!)
                                        findNavController().navigateUp()
                                    }
                                ) { Text(text = "Delete") }
                            }
                        }
                    }
                }
            }
        }
    }
}
