package com.example.todo.view.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Snackbar
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.todo.R
import com.example.todo.model.entity.Task
import com.example.todo.ui.theme.ToDoTheme
import com.example.todo.viewmodel.TaskListViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Dashboard fragment.
 *
 * @constructor Create empty Dashboard fragment
 */
@AndroidEntryPoint
class DashboardFragment : Fragment() {
    private val taskListViewModel by viewModels<TaskListViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by taskListViewModel.taskState.collectAsState()
                ToDoTheme {
                    taskListViewModel.getTasks()
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            if (state.isLoading) { CircularProgressIndicator() }
                            Button(
                                onClick = {
                                    findNavController()
                                        .navigate(R.id.action_dashboardFragment_to_createTaskFragment)
                                }
                            ) { Text(text = "Add a new task") }
                            if (state.taskList.all { it.isCompleted == true } && state.taskList.isNotEmpty()) {
                                Snackbar() { Text(text = "Congrats on completing your tasks!") }
                            }
                            LazyColumn {
                                items(state.taskList) { task ->
                                    if (task.isCompleted == false) {
                                        DisplayTask(task) {
                                            val directions = DashboardFragmentDirections
                                                .actionDashboardFragmentToTaskFragment(it)
                                            findNavController().navigate(directions)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun DisplayTask(task: Task, onclick: (Int) -> Unit) {
    Card(shape = RoundedCornerShape(10.dp)) {
        Text(text = "${task.name}")
        Button(onClick = { onclick(task.id) }) {
            Text(text = "Expand")
        }
    }
}
