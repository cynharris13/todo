package com.example.todo.view.dashboard

import com.example.todo.model.entity.Task

/**
 * Task list state.
 *
 * @property isLoading
 * @property taskList
 * @constructor Create empty Task list state
 */
data class TaskListState(
    val isLoading: Boolean = false,
    val taskList: List<Task> = emptyList()
)
