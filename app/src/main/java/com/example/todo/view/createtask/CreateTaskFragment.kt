package com.example.todo.view.createtask

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.todo.model.entity.Task
import com.example.todo.ui.theme.ToDoTheme
import com.example.todo.viewmodel.NewTaskViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 * Use the [CreateTaskFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class CreateTaskFragment : Fragment() {
    private val newTaskViewModel by viewModels<NewTaskViewModel>()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val completed by newTaskViewModel.isFinished.collectAsState()
                ToDoTheme() {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        val taskName = remember { mutableStateOf("") }
                        val taskDescription = remember { mutableStateOf("") }
                        OutlinedTextField(
                            value = taskName.value,
                            onValueChange = { newText -> taskName.value = newText },
                            label = { Text("Task Name") },
                            colors = TextFieldDefaults.textFieldColors(textColor = Color.Black)
                        )
                        OutlinedTextField(
                            value = taskDescription.value,
                            onValueChange = { newText -> taskDescription.value = newText },
                            label = { Text("Task Description") },
                            colors = TextFieldDefaults.textFieldColors(textColor = Color.Black)
                        )
                        Button(
                            onClick = {
                                val newTask = Task(
                                    name = taskName.value,
                                    description = taskDescription.value,
                                    isCompleted = false
                                )
                                newTaskViewModel.addTask(newTask)
                                Toast.makeText(requireContext(), "New task created!", Toast.LENGTH_LONG).show()
                            }
                        ) { Text("Create Task") }
                        if (completed) { findNavController().navigateUp() }
                    }
                }
            }
        }
    }
}
