package com.example.todo.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todo.model.TaskRepo
import com.example.todo.model.entity.Task
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * New task view model.
 *
 * @property repo
 * @constructor Create empty New task view model
 */
@HiltViewModel
class NewTaskViewModel @Inject constructor(private val repo: TaskRepo) : ViewModel() {
    private val _isFinished = MutableStateFlow(false)
    val isFinished: StateFlow<Boolean> get() = _isFinished

    /**
     * Add task.
     *
     * @param task
     */
    fun addTask(task: Task) = viewModelScope.launch {
        repo.addTask(task)
        _isFinished.update { true }
    }
}
