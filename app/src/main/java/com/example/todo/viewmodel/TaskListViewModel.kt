package com.example.todo.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todo.model.TaskRepo
import com.example.todo.view.dashboard.TaskListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Task list view model.
 *
 * @property repo
 * @constructor Create empty Task list view model
 */
@HiltViewModel
class TaskListViewModel @Inject constructor(val repo: TaskRepo) : ViewModel() {
    private val _tasks = MutableStateFlow(TaskListState())
    val taskState: StateFlow<TaskListState> get() = _tasks

    /**
     * Get tasks.
     *
     */
    fun getTasks() = viewModelScope.launch {
        _tasks.update { it.copy(isLoading = true) }
        val taskList = repo.getTasks()
        _tasks.update { it.copy(isLoading = false, taskList = taskList) }
    }
}
