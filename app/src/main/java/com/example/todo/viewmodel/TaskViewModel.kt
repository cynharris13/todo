package com.example.todo.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todo.model.TaskRepo
import com.example.todo.model.entity.Task
import com.example.todo.view.details.TaskState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Task view model.
 *
 * @property repo
 * @constructor Create empty Task view model
 */
@HiltViewModel
class TaskViewModel @Inject constructor(private val repo: TaskRepo) : ViewModel() {
    private val _task = MutableStateFlow(TaskState())
    val taskState: StateFlow<TaskState> get() = _task

    /**
     * Get task.
     *
     * @param id
     */
    fun getTask(id: Int) = viewModelScope.launch {
        _task.update { it.copy(isLoading = true) }
        val task = repo.getTask(id)
        _task.update { it.copy(isLoading = false, task = task) }
    }

    /**
     * Update task.
     *
     * @param task
     */
    fun updateTask(task: Task) = viewModelScope.launch {
        repo.updateTasks(task)
        _task.update { it.copy(task = task) }
    }

    /**
     * Delete task.
     *
     * @param task
     */
    fun deleteTask(task: Task) = viewModelScope.launch {
        repo.deleteTask(task)
    }
}
