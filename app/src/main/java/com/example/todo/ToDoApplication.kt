package com.example.todo

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp

/**
 * To do override of the application class.
 *
 * @constructor Create empty To do application
 */
@HiltAndroidApp
class ToDoApplication : Application() {
    init {
        instance = this
    }
    companion object {
        private var instance: ToDoApplication? = null

        /**
         * App context.
         *
         * @return
         */
        fun appContext(): Context {
            return instance!!.applicationContext
        }
    }
}
