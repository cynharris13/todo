package com.example.todo.viewmodel

import com.example.todo.model.TaskRepo
import com.example.todo.model.entity.Task
import com.example.todo.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class TaskListViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val repo = mockk<TaskRepo>()
    private val taskListVM = TaskListViewModel(repo)

    @Test
    @DisplayName("Tests getting the task list")
    fun testGetTasks() = runTest(extension.dispatcher) {
        // given
        val expected = listOf(Task(name = "to do", description = "something", isCompleted = false))
        coEvery { repo.getTasks() } coAnswers { expected }

        // when
        taskListVM.getTasks()

        // then
        Assertions.assertFalse(taskListVM.taskState.value.isLoading)
        Assertions.assertEquals(expected, taskListVM.taskState.value.taskList)
    }
}
