package com.example.todo.viewmodel

import com.example.todo.model.TaskRepo
import com.example.todo.model.entity.Task
import com.example.todo.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class NewTaskViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val repo = mockk<TaskRepo>()
    private val newTaskVM = NewTaskViewModel(repo)

    @Test
    @DisplayName("Tests adding a task")
    fun testGetTasks() = runTest(extension.dispatcher) {
        // given
        val expected = Task(name = "to do", description = "something", isCompleted = false)
        coEvery { repo.addTask(expected) } coAnswers { }

        // when
        newTaskVM.addTask(expected)

        // then
        Assertions.assertTrue(newTaskVM.isFinished.value)
    }
}
