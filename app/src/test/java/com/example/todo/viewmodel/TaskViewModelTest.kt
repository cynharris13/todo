package com.example.todo.viewmodel

import com.example.todo.model.TaskRepo
import com.example.todo.model.entity.Task
import com.example.todo.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class TaskViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val repo = mockk<TaskRepo>()
    private val taskVM = TaskViewModel(repo)

    @Test
    @DisplayName("Tests getting a task from id")
    fun testGetTask() = runTest(extension.dispatcher) {
        // given
        val expected = Task(name = "to do", description = "something", isCompleted = false)
        coEvery { repo.getTask(0) } coAnswers { expected }

        // when
        taskVM.getTask(0)

        // then
        Assertions.assertFalse(taskVM.taskState.value.isLoading)
        Assertions.assertEquals(expected, taskVM.taskState.value.task)
    }

    @Test
    @DisplayName("Tests updating a task")
    fun testUpdateTask() = runTest(extension.dispatcher) {
        // given
        val expected = Task(name = "to do", description = "something", isCompleted = false)
        coEvery { repo.updateTasks(expected) } coAnswers { }

        // when
        taskVM.updateTask(expected)

        // then
        Assertions.assertEquals(expected, taskVM.taskState.value.task)
    }
}
