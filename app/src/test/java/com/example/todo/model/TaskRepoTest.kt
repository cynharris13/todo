package com.example.todo.model

import com.example.todo.model.entity.Task
import com.example.todo.model.local.TaskDB
import com.example.todo.model.local.dao.TaskDAO
import com.example.todo.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class TaskRepoTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val db = mockk<TaskDB>()
    private val dao = mockk<TaskDAO>()

    @BeforeEach
    fun setup() {
        coEvery { db.taskDAO() } coAnswers { dao }
    }

    @Test
    @DisplayName("Tests retrieving the task list")
    fun testGetTaskList() = runTest(extension.dispatcher) {
        // given
        val repo = TaskRepo(db)
        val expected = listOf(Task(name = "to do", description = "something", isCompleted = false))
        coEvery { dao.getTasks() } coAnswers { expected }

        // when
        val actual = repo.getTasks()

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    @DisplayName("Tests retrieving a task given an ID")
    fun testGetTask() = runTest(extension.dispatcher) {
        // given
        val repo = TaskRepo(db)
        val expected = Task(name = "to do", description = "something", isCompleted = false)
        coEvery { dao.findByID(0) } coAnswers { expected }

        // when
        val actual = repo.getTask(0)

        // then
        Assertions.assertEquals(expected, actual)
    }
}
